<?php

define("SITE_TITLE", "پروژه تستی من");
define("PAGE_SIZE", 3);
define("ENCODING", "UTF-8");


$styles = '
    <link rel="stylesheet" href="css/milligram-rtl.min.css">
    <link rel="stylesheet" href="css/style.css">
    
';

$posts = [
    ["پست شماره 1","1 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 2","2 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 3","3 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 4","4 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 5","5 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 6","6 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 7","7 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 8","8 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 9","9 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"],
    ["پست شماره 10","10 A animi dicta distinctio dolorem eum, fuga ipsam, laudantium maxime minus, obcaecati op"]
];

$PageCount = ceil(sizeof($posts) / PAGE_SIZE);
$index = ($_GET['page']-1) * PAGE_SIZE;
