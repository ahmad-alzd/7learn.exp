<?php

include 'data.php';

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="<?php ENCODING ?>">
    <?php echo $styles; ?>
    <title><?php echo SITE_TITLE ?></title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <h1>وب سایت من</h1>
    </div>

    <div class="row">
        <div class="sidebar column column-25 sidebar">
            <h1>Sidebar</h1>
        </div>
        <div class="main column column-75">

            <?php for ($j = $index; $j < $index + PAGE_SIZE; $j++) : ?>
            <?php if (!empty($posts[$j])) : ?>
            <div class="post">
                <h6><?php echo $posts[$j][0]; ?></h6>
                <p><?php echo $posts[$j][1]; ?></p>
            </div>
            <?php endif; ?>
            <?php endfor; ?>

            <div class="row">
                <?php for ($i = 1;$i <= $PageCount;$i++) : ?>
                    <a class="button" href="?page=<?php echo $i ?>"><?php echo $i ?></a>
                <?php endfor; ?>
            </div>
        </div>



    </div>
</div>
</body>
</html>
